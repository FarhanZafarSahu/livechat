var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
        return res.status(200).json({});
    }
    next();
});
server.listen(8890);
var clients = [];

io.on('connection', function (socket) {

  console.log("client connected server side");
  socket.on('addUser', function (data) {
    console.log(data)
    console.log('User Connected: ' + data.userId);
    if (typeof clients[data.userId] == 'undefined')
        clients[data.userId] = [];
    socket.emit('onlineUsers', getConnectedUsers(clients));
    for (var j = 0; j < clients[data.userId].length; j++) {
        if (clients[data.userId][j].socket == socket.id)
            clients[data.userId].splice(j, 1);
    }
    clients[data.userId].push({ "socket": socket.id });
    clients[data.userId]['status'] = false;
    console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@222');
    console.log('userID+ '+data.userId+" socketId+ "+socket.id);
    console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@222');
    if (typeof clients[data.userId] != 'undefined') {
        for (var i = 0; i < clients[data.userId].length; i++) {
            socket.broadcast.to(clients[data.userId][i].socket).emit('useradded', data);
        }
    }
});
socket.on('sendMessage', async function (data, callback) {
    console.log(data);
    let from_user_role_id = 2;
    if(data.data.from_user_role_id!=undefined && data.data.from_user_role_id!=null && data.data.from_user_role_id!="")
        from_user_role_id = data.data.from_user_role_id;
    additionalInfo = {
        type: "message",
        thread_id: data.data.thread_id,
        from_user_id: data.data.from_user_id,
        from_user_name: data.data.from_user_name,
        profile_image: data.data.profile_image,
        click_action: "FLUTTER_NOTIFICATION_CLICK",
        from_user_role_id: from_user_role_id
    }
    let body,title = data.data.from_user_name;
    if(data.data.type == "Text"){
        body = data.data.message;
    }
    else if(data.data.type == "Image"){
        body = "Shared an image.";
    }
    else if(data.data.type == "Voice"){
        body = "Shared a voice note.";
    }
    else if(data.data.type == "Story"){
        body = "Replied on your story.";
    }
    else{
        body = "Shared a post.";
    }
    let message = await saveMessage(data);
    markThreadUnread(data);
    generateNotifications(data.userId, title, body, additionalInfo);
    if (typeof clients[data.userId] != 'undefined') {
        for (var i = 0; i < clients[data.userId].length; i++) {
            socket.broadcast.to(clients[data.userId][i].socket).emit('message', data);
        }
        if (typeof clients[data.from] != 'undefined') {
            for (var i = 0; i < clients[data.from].length; i++) {
                if (clients[data.from][i].socket != socket.id) {
                    socket.broadcast.to(clients[data.from][i].socket).emit('message', data);
                }
            }
        }
    }
    if(callback!=undefined){
        callback({
            "status":true,
            "data": {
                "id": message.insertId,
                "message_number": data.data.message_number
            }
        });
    }
});
//   var redisClient = redis.createClient();
//   redisClient.subscribe('message');

//   redisClient.on("message", function(channel, data) {
    // console.log("mew message add in queue "+ data['message'] + " channel");
//     socket.emit(channel, data);
//   });

  socket.on('disconnect', function() {
    // redisClient.quit();
  });

});
function getConnectedUsers(clients) {
    connectedUsers = [];
    count = 0;
    for (var i in clients) {
        if (clients[i]['status'] === true) {
            connectedUsers[count] = i;
            ++count;
        }
    }
    return connectedUsers;
}
