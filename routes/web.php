<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => [App\Http\Middleware\Cros::class]], function (){
// Route::group(['middleware' => 'web'], function () {
    // Route::auth();
    // Route::get('/home', 'HomeController@index');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('sendmessage',[App\Http\Controllers\chatController::class, 'sendMessage']);
});
